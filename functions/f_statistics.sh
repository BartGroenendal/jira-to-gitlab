function jira_stats {
  print_log '    Collecting Jira statistics.'
  export JIRA_PROJECTS=$(cat ${WORKING_DIR}/jira_projects.json | jq '.values[] | .name' | wc -l)
  export JIRA_ISSUES=$(cat ${WORKING_DIR}/jira_projects.json | jq '.values[] | (.insight.totalIssueCount)' | paste -sd+ | bc)
  export JIRA_EPICS=$(find ${WORKING_DIR} -name '*-*.json' | egrep -v 'gitlab|extended|attachments' | xargs jq .fields.issuetype.name | grep Epic | wc -l)
  export JIRA_SPRINTS=$(cat ${WORKING_DIR}/*/SPRINTS/sprints_sorted.csv | grep -v '^~' | wc -l)
}


function gitlab_stats {
  print_log '    Collecting GitLab statistics.'
  export GITLAB_PROJECTS=$(find ${WORKING_DIR} -name 'gitlab_project.json' | xargs xargs grep '^{"id"' | wc -l)
  export GITLAB_GROUPS=$(find ${WORKING_DIR} -name 'gitlab_group.json' | xargs xargs grep '^{"id"' | wc -l)
  export GITLAB_ISSUES=$(find ${WORKING_DIR} -name 'gitlab_issue.json' | xargs grep '^{"id"' | wc -l)
  export GITLAB_EPICS=$(find ${WORKING_DIR} -name 'gitlab_issue_promoted_epic.json' | xargs grep '{"commands_changes"' | wc -l)
  export GITLAB_MILESTONES=$(find ${WORKING_DIR} -name 'milestone_*' | grep -v 'closed.json' | xargs grep '^{"id":' | wc -l)
}


function script_stats {
  print_log '    Collecting script statistics.'
  export END_DATE=$(date +%s)
  export RUN_TIME_S=$((END_DATE-START_DATE))
  export RUN_TIME_M=$((RUN_TIME_S/60))
  export IMPORT_SIZE="$(du -hs ${WORKING_DIR} | cut -f 1)"
}


function statistics {
  jira_stats
  gitlab_stats
  script_stats
  print_log '    **Statistics**'
  print_log "    - Jira:"
  print_log "      - ${JIRA_PROJECTS} projects"
  print_log "      - ${JIRA_ISSUES} issues"
  print_log "      - ${JIRA_EPICS} epics"
  print_log "      - ${JIRA_SPRINTS} sprints"
  print_log "    - GitLab:"
  print_log "      - ${GITLAB_GROUPS} groups"
  print_log "      - ${GITLAB_PROJECTS} projects"
  print_log "      - ${GITLAB_ISSUES} issues"
  print_log "      - ${GITLAB_EPICS} epics"
  print_log "      - ${GITLAB_MILESTONES} milestones (sprints)"
  print_log "    - Script:"
  print_log "      - Duration (minutes): ${RUN_TIME_M}"
  print_log "      - Import size: ${IMPORT_SIZE}"
}

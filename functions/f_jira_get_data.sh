function get_issue_data {
  # Set issue key
  I_KEY="${1}"
  
  # Set variables for storing data related to each issue
  export ISSUE_FOLDER="${PROJECT_WORKING_DIR}/${I_KEY}"
  export ISSUE_FOLDER_ATTACHMENTS="${ISSUE_FOLDER}/attachments"

  # Create the issue folders
  mkdir -p ${ISSUE_FOLDER}
  mkdir -p ${ISSUE_FOLDER_ATTACHMENTS}

  # Get the JSON for this ticket (metadata, comments, reference to attachments, etc.).
  print_log "     ${I_KEY}: Downloading JSON"
  curl \
    --silent \
    --request GET \
    --url "${JIRA_URL}/rest/api/3/issue/${I_KEY}" \
    --user "${JIRA_LOGIN}:${JIRA_TOKEN}" \
    --header 'Accept: application/json' \
    --output ${ISSUE_FOLDER}/${I_KEY}.json

  # Get the extended JSON for this ticket
  print_log "     ${I_KEY}: Downloading extended JSON"
  curl \
    --silent \
    --request GET \
    --url "${JIRA_URL}/rest/api/3/issue/${I_KEY}?expand=renderedFields,names,schema,operations,editmeta,changelog,versionedRepresentations" \
    --user "${JIRA_LOGIN}:${JIRA_TOKEN}" \
    --header 'Accept: application/json' \
    --output ${ISSUE_FOLDER}/${I_KEY}-extended.json

  # If present, down all attachments (for this we need to work from the attachments folder)
  print_log "     ${I_KEY}: Downloading attachments"
  cd ${ISSUE_FOLDER_ATTACHMENTS}
  if cat ${ISSUE_FOLDER}/${I_KEY}.json | jq .fields.attachment[]?.content > /dev/null 2>&1
  then
    cat ${ISSUE_FOLDER}/${I_KEY}.json \
    | jq .fields.attachment[]?.content \
    | xargs -I{} sh -c 'curl --location --silent --user $1 --url $2 --output $(basename $2)' - "${JIRA_LOGIN}:${JIRA_TOKEN}" {}
  fi

  # Return to the RUN directory
  cd ${RUN_DIR}
}


function get_jira_projects_data {
  # Iterate across the CSV file: col1 = key, col2 = name
  while IFS=, read -r col1 col2
  do
    # Set the Jira project name: $J_KEY
    set_j_name "${col1}" "${col2}"

    # Set the Jira project key: $J_KEY
    set_j_key "${col1}" "${col2}"

    # Project working dir
    export PROJECT_WORKING_DIR="${WORKING_DIR}/${J_KEY}"

    # Create the working directory for this Jira project
    print_log "   ${J_KEY}: Create project dir ${PROJECT_WORKING_DIR}"
    mkdir -p ${PROJECT_WORKING_DIR}

    # Get all issue keys for this project
    # Note: If there's an error with the J_KEY, this is the step that will generate an error!
    print_log "   ${J_KEY}: Get all issue keys"
    curl \
      --silent \
      --request GET \
      --url "${JIRA_URL}/rest/api/3/search?jql=project=${J_KEY}&fields=key&maxResults=10000" \
      --user "${JIRA_LOGIN}:${JIRA_TOKEN}" \
      --header 'Accept: application/json' \
      --output ${PROJECT_WORKING_DIR}/issue_keys.json

    # Put the issue keys into an array: $ISSUE_KEYS
    set_issue_keys

    # Get all data for each issue
    for i in ${ISSUE_KEYS}
    do
      # Rate limiter
      waitforjobs

      # Run the downloads for this issue in a background job
      get_issue_data ${i} &
    done

    # Wait for all background jobs to finish for this project
    wait
  done < ${WORKING_DIR}/jira_projects.csv
}

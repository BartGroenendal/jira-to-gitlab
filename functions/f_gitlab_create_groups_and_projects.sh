function create_gitlab_group {
  # Create new GitLab group, under the `GITLAB_PARENT_GROUP`
  curl \
    --silent \
    --request POST \
    --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
    --url ${GITLAB_URL}/api/v4/groups \
    --data-urlencode "path=${J_NAME}" \
    --data-urlencode "name=${J_NAME}" \
    --data-urlencode "visibility=${GITLAB_VISIBILITY}" \
    --data-urlencode "parent_id=${GITLAB_PARENT_GROUP}" \
    --output ${PROJECT_WORKING_DIR}/gitlab_group.json
  
  # Get the new group ID
  export GITLAB_GROUP_ID=$(cat ${PROJECT_WORKING_DIR}/gitlab_group.json | jq .id)
}


function create_gitlab_project {
  # Create a new project under the new group
  curl \
    --silent \
    --request POST \
    --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
    --url ${GITLAB_URL}/api/v4/projects \
    --data-urlencode "path=${J_KEY}" \
    --data-urlencode "visibility=${GITLAB_VISIBILITY}" \
    --data-urlencode "namespace_id=${GITLAB_GROUP_ID}" \
    --output ${PROJECT_WORKING_DIR}/gitlab_project.json
}


function create_gitlab_groups_and_projects {
  # Iterate across the CSV file: col1 = key, col2 = name
  while IFS=, read -r col1 col2
  do
    # Set the Jira project name: $J_KEY
    set_j_name "${col1}" "${col2}"

    # Set the Jira project key: $J_KEY
    set_j_key "${col1}" "${col2}"

    # Project working dir
    export PROJECT_WORKING_DIR="${WORKING_DIR}/${J_KEY}"
    
    # Create GitLab group
    print_log "   Creating GitLab group ${J_NAME} under parent group ID ${GITLAB_PARENT_GROUP}"
    create_gitlab_group

    # Create GitLab project
    print_log "   Creating GitLab project ${J_KEY} under group ${J_NAME} (ID ${GITLAB_GROUP_ID})"
    create_gitlab_project
  done < ${WORKING_DIR}/jira_projects.csv
}

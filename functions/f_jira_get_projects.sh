function get_jira_projects {
  # Get all Jira projects
  curl \
    --silent \
    --request GET \
    --url "${JIRA_URL}/rest/api/3/project/search?expand=insight&orderBy=issueCount&maxResults=10000" \
    --user "${JIRA_LOGIN}:${JIRA_TOKEN}" \
    --header 'Accept: application/json' \
    --output ${WORKING_DIR}/jira_projects.json

  # Create ${WORKING_DIR}/jira_projects.csv
  cat ${WORKING_DIR}/jira_projects.json | jq -r '.values[] | .key + "," + .name' > ${WORKING_DIR}/jira_projects.csv
}
